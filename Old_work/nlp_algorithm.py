import nltk
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer 
ps = PorterStemmer()
from nltk.stem import SnowballStemmer
sbst=SnowballStemmer('english')
from nltk.corpus import stopwords
stopWords = set(stopwords.words('english'))
from nltk import FreqDist
from nltk.util import bigrams, trigrams, ngrams
import json
#temp array to append tokenized data
temp_array=[]

temp_str=[]

#Will work effeciently when using data from individual files. (Hopefully)
#read json file
jsonfile = open('./formated_text_data/Business_Technology_Platform/Analytics.json')
# jsonfile = open('data_set.json')
jsonstr = jsonfile.read()
jdata = json.loads(jsonstr)
data = list(jdata.values())
count = 0

#Sorting elements
for elems in data[0]:
    temp_str.append(elems.lower())

#tokenizing data from array list
for elems in temp_str:
    tokenized_data = word_tokenize(elems.lower())
    temp_array.append(tokenized_data)

#Nd array to 1d array
ndto1d=[]
for elems in temp_array:
    for elem in elems:
        ndto1d.append(elem)

#initialize wordList
wordList = []
#initialize wordListStemmed
wordListStemmed = []
#Loop to append words
for w in ndto1d: 
    wordList.append(w)
    wordListStemmed.append(ps.stem(w))
    # print(w + ':' + sbst.stem(w))

wordListFiltered = []

#Filtering by stop words
for w in wordList:
    if w not in stopWords:
        wordListFiltered.append(w)

fdist=FreqDist(wordListFiltered)

f_dist = list(filter(lambda x: x[1]<=10,fdist.items()))

# list_concat = ''
# for elems in wordList:
#     str1 = ''.join(elems)
#     list_concat = list_concat + '\n' + str1

#Capture less frequent words for further analysis

words=[]
for (word, freq) in f_dist:
    words.append(word)

q_bigrams = list(nltk.trigrams(words))
print(q_bigrams)