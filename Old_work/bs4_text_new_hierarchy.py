from bs4 import BeautifulSoup
from bs4.element import Comment
import urllib.request
import json
import csv

def visible_tag(element):
    if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]', 'footer']:
        return False
    if isinstance(element, Comment):
        return False
    return True

def text_from_html(body):
    soup = BeautifulSoup(body, 'html.parser')
    texts = soup.findAll(text=True)
    visible_texts = filter(visible_tag, texts)  
    return u" ".join(t.strip() for t in visible_texts)

def removeDuplicates(json):
    return list(dict.fromkeys(json))

def main_func():
    with open('product_list.csv', 'r') as file:
        reader = csv.reader(file)
        next(reader, None)
        for row in reader:
            name = row[0] #Assigning product name variable
            url = row[1]  #Assigning product url to variable
            html = urllib.request.urlopen(url).read()
            data = text_from_html(html) #Extracting relevant text from webpage
            removedBlanks = data.split("  ") #Removing white spaces
            filteredData = list(filter(None, removedBlanks)) #Adding filter
            filteredData_ = removeDuplicates(filteredData) #Removing duplicate values if any from the result
            # print(filteredData_)
            name_ = name.replace(" ","_") #Replacing whitespace in name string with '_' for easy name representation on dist
            nameRep = name_.replace("/", "_")
            #Writing result to JSON
            with open('./New_hierarchy/' + nameRep + '.json', 'w') as f:
                json.dump({nameRep: filteredData_}, f, sort_keys=True, indent=4, separators=(',', ': '))
    
#Main func
if __name__ == '__main__':
    main_func()
