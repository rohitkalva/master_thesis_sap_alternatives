import json
import os
import pathlib

def getListOfFiles(dirName):
    # create a list of file and sub directories 
    # names in the given directory 
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory 
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            allFiles.append(fullPath)
                
    return allFiles        
 
 
def dirCall():    
    dirName = './json_files'
    # Get the list of all files in directory tree at given path
    listOfFiles = getListOfFiles(dirName)
    return listOfFiles

def removeDuplicates(json):
  return list(dict.fromkeys(json))

def main_fun():
    listOfFiles = dirCall()
    
    for elem in listOfFiles:
        fileName = os.path.basename(elem)
        keyName = fileName.split(".json")
        #Reading JSON file from Path
        with open(elem) as jsonFile:
            jsonObject = json.load(jsonFile)
            jsonFile.close()
        
        # Assigning key name from file name
        temp_keyname = keyName[0]
        #Assigning data to temp variable using the key name
        tempJsonData = jsonObject[temp_keyname]
        #Removing blank spaces from the temp data
        removeBlanks = tempJsonData.split("  ")
        #Removing null values from the temp data list
        dataFilter = list(filter(None, removeBlanks))
        #Removing duplicate keyword values
        dataFilter_ = removeDuplicates(dataFilter)
        # Assigning directory to store the new files
        dirName = os.path.dirname(elem).split('./json_files/')
        #Formating data in JSON format
        json_data = { keyName[0] : dataFilter_}
        path_ = './formated_text_data/' + dirName[1]
        #Check if path exists. If not exists create new
        pathlib.Path(path_).mkdir(parents=True, exist_ok=True) 
        #Writing JSON file with same file name to a different directory
        with open(path_ + '/' + os.path.basename(elem), 'w') as jsonFile:
            json.dump(json_data, jsonFile)
            jsonFile.close()


#Executing main function
if __name__ == '__main__':
    main_fun()