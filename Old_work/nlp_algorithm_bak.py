import nltk
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer 
ps = PorterStemmer()
from nltk.stem import SnowballStemmer
sbst=SnowballStemmer('english')
from nltk.corpus import stopwords
stopWords = set(stopwords.words('english'))
from nltk import FreqDist
from nltk.util import bigrams, trigrams, ngrams
import json
import pandas as pd

def lower_elements(temp_str):
    temp_array = []
    for elems in temp_str:
        temp_array.append(elems.lower())
    return temp_array


def tokenize(temp_str):
    temp_array = []
    # #tokenizing data from array list
    for elems in lower_elements(temp_str):
        tokenized_data = word_tokenize(elems.lower())
        temp_array.append(tokenized_data)       
    
    return temp_array

#FlattenArray not needed for text extracted from PDF
def flattenArray(temp_str):
    ndto1d = []
    for elems in temp_str:
        for elem in elems:
            ndto1d.append(elem)
    return ndto1d

def toStemm(temp_str):
    wordList = []
    #initialize wordListStemmed
    wordListStemmed = []
    #Loop to append words
    for w in temp_str: 
        # wordList.append(w)
        wordListStemmed.append(ps.stem(w))
    return wordListStemmed

def removeStopwords(temp_str):
    #Filtering by stop words
    removedStopWords = []
    for w in temp_str:
        if w not in stopWords:
            removedStopWords.append(w)
    return removedStopWords

def frequency_distribution(temp_str,count):

    fdist=FreqDist(temp_str)
    f_dist = list(filter(lambda x: x[1]<=count,fdist.items()))
    return f_dist


def main_func():
    jsonfile = open('./Data/PDFs/SAP_Access_Control.json')
    jsonstr = jsonfile.read()
    jdata = json.loads(jsonstr)
    json_values = list(jdata.values())
    # print(jdata)
    arrTokenize = tokenize(json_values)
    frame = pd.DataFrame(arrTokenize[0])
    print("Schema:\n\n",frame.shape)
    # stemmStr  = toStemm(arrTokenize[0]) #Words not being stemmed
    # print(flatArray[0])
    # removedStopWords = removeStopwords(flatArray)
    removedStopWords = removeStopwords(arrTokenize[0])

    fdist = frequency_distribution(removedStopWords,10)
    
    # for elems in fdist:
    #     print(elems)


if __name__ == '__main__':
    main_func()