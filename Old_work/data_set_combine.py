import json
import os

#Initializing temp array
result = []
def getListOfFiles(dirName):
    # create a list of file and sub directories 
    # names in the given directory 
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory 
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            allFiles.append(fullPath)
                
    return allFiles        
 
def dirCall():    
    dirName = './formated_text_data' 
    # Get the list of all files in directory tree at given path
    listOfFiles = getListOfFiles(dirName)
    return listOfFiles

def main_func():
    paths = dirCall()
    for elems in paths:
        #Opening the file
        with open(elems, "r") as infile:
            result.append(json.load(infile))
        #Writing the file
        with open("data_set.json", "w") as outfile:
            json.dump(result, outfile)

#Main func
if __name__ == '__main__':
    main_func()