from bs4 import BeautifulSoup
from bs4.element import Comment
import urllib.request
import json

def visible_tag(element):
    if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]', 'footer']:
        return False
    if isinstance(element, Comment):
        return False
    return True


def text_from_html(body):
    soup = BeautifulSoup(body, 'html.parser')
    texts = soup.findAll(text=True)
    visible_texts = filter(visible_tag, texts)  
    return u" ".join(t.strip() for t in visible_texts)

#Replace target urls from https://www.sap.com/products.html

url = 'https://www.sap.com/products/s4hana-erp/features.html'
html = urllib.request.urlopen(url).read()

#Replace file name, folder name and json key according to the target url.
with open('./json_files/Business_Technology_Platform/Intelligent_Technologiesw.json', 'w') as f:
  json.dump({"Intelligent_Technologies": text_from_html(html)}, f, sort_keys=True, indent=4, separators=(',', ': '))  
