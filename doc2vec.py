from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from nltk.tokenize import word_tokenize
from comparision import fetch_data_df
import time
import pandas as pd
import os.path
import json
start_time = time.time()

# os.chdir('Models')
unavailable_list = ['NA']

def master_func(sap_product_category, sap_desc, product_features):
    # Initialize variable
    temp_str = "SAP"
    # part of sql query
    add_string = "AND a.product_name not like '%%%%%s%%%%' AND (" % (temp_str)
    # Sql query part to append project_features in search for the count of product features entered with initial phrase.
    add_project_features = "a.project_features like '%%%%%s%%%%' " 
    query_ = ''
    # appending the string with search for project_features
    for i in range(len(product_features)):
        if i == 0:
            query_ = add_project_features % product_features[i].lstrip(' ')
        else:
            query_ = query_ + 'OR ' + add_project_features % product_features[i].lstrip(' ')

# Conditional check to select appropriate SQL query to fetch data
    if sap_product_category == 'Enterprise Management':
        query = "select a.product_name, a.project_description, a.project_features, a.page_text, a.project_last_update, b.project_category, a.Pricing_Information \
                        from alternative_products a JOIN alternate_category b \
                        WHERE b.project_category = 'ERP' AND a.product_name = b.product_name AND a.project_last_update > '2017-01-01' " + add_string + query_  + ');'
    elif sap_product_category == 'Customer Relationship Management':
        query = "select a.product_name, a.project_description, a.project_features, a.page_text, a.project_last_update, b.project_category, a.Pricing_Information \
                        from alternative_products a JOIN alternate_category b \
                        WHERE b.project_category = 'CRM' AND a.product_name = b.product_name AND a.project_last_update > '2017-01-01' " + add_string + query_  + ');'
    elif sap_product_category not in unavailable_list:
        query = "select a.product_name, a.project_description, a.project_features, a.page_text, a.project_last_update, b.project_category, a.Pricing_Information \
                    from alternative_products a JOIN alternate_category b \
                    WHERE b.project_category ='" + sap_product_category + "' AND a.product_name = b.product_name AND a.project_last_update > '2017-01-01' " + add_string + query_  + ');'
    else:
        print('Not right category')

    # Reading data from DB
    doc_df = fetch_data_df(query)
    tokenized_doc = []
    # Tokenizing the strings
    for d in doc_df['project_description']:
        tokenized_doc.append(word_tokenize(d.lower()))

    tagged_data = [TaggedDocument(d, [i]) for i, d in enumerate(tokenized_doc)]

    # Train doc2vec model
    model = Doc2Vec(tagged_data, vector_size=20, window=2, min_count=1, workers=4, epochs = 100)
    # Save trained doc2vec model
    # Check for existing model in local path
    model_name = sap_product_category.replace(" ", "_")

    # Check if model exists locally
    if os.path.isfile(model_name + '.model') == True:
        print("Test model found")
        model= Doc2Vec.load(model_name + '.model')
        print("Test Model read successfully")
        ## Print model vocabulary
        model.wv.vocab
    else:
        model.save(model_name + '.model')
        print("Test model saved successfully")
        # Load saved doc2vec model
        model= Doc2Vec.load(model_name + '.model')
        print("Test Model read successfully")
        ## Print model vocabulary
        model.wv.vocab

    # Tokenizing test doc ( SAP description)
    test_doc = word_tokenize(sap_desc.lower())
    model_result = model.docvecs.most_similar(positive=[model.infer_vector(test_doc)],topn=len(doc_df['product_name']))
    # print(model_result)

    # Initializing temp variables to assign the row number and similarity value respectively. 
    temp_a = []
    temp_b = []
    for i in model_result:
        a = str(i)
        a = a.replace("(", "")
        a = a.replace(")", "")
        a = a.split(',')
        
        temp_a.append(int(a[0]))
        temp_b.append(a[1])
    # Temp DF with extracted row number and the similarity
    df = pd.DataFrame(list(zip(temp_a, temp_b)), columns =['S No', 'similarity']) 
    # Sorting in ascending order. 
    df_1 = df.sort_values(by='S No', ascending=True)
    # Resetting index
    df_1 = df_1.reset_index(drop=True)

    # Adding similarity column to main df
    doc_df['Similarity'] = df_1['similarity']
    # Re-writing data-type of Similarity column in DataFrame
    doc_df[["Similarity"]] = doc_df[["Similarity"]].apply(pd.to_numeric)
    # Sorting values in DF in descending order. 
    doc_df.sort_values(by='Similarity', ascending=False, inplace=True)
    
    # doc_df = doc_df.reset_index(drop=True)
    return doc_df


def json_formatter(df):

    # del df['project_features']  # deleting project_features column for better presentation
    del df['page_text']  # deleting page_textcolumn for better presentation

    # formatting resultant df to json string. head limit for number of results
    json_df = df.head(10).to_json(orient='records')

    print("Task completed in %s minutes " % ((time.time() - start_time) / 60))
    return json_df

def doc2vec(product_name, product_features):

    try:
        query = "select * from sap_data where product_name = '" + product_name + "';"
        print(query)
        sap_df = fetch_data_df(query)
        print(sap_df)
        sap_page_text = sap_df['Page_Text'][0]

        sap_file_text = sap_df['File_Text'][0]

        sap_product_category = sap_df['Product_Category'][0]

        sap_desc = ''

        if sap_file_text == 'Not Available':
            sap_desc = sap_page_text    

        else:
            sap_desc = sap_file_text
        
        if sap_product_category not in unavailable_list:
            compared_df = master_func(sap_product_category, sap_desc, product_features)
            # print(compared_df)
            json_doc = json_formatter(compared_df)
            
            return json_doc
        else:
            json_resp = {
                        'message': 'Suitable alternative not found'
                        }
            
            print("Task completed in %s minutes " % ((time.time() - start_time) / 60))
            return json.dumps(json_resp)

    except:
        json_resp = {
                        'message': 'Error Occured'
                        }
        print("Task completed in %s minutes " % ((time.time() - start_time) / 60))
        print("Exception")
        return json.dumps(json_resp)
    



if __name__ == '__main__':
    doc2vec('SAP ERP')