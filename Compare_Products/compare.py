#Code to Map two columns of CSV and save  corresponding values to new CSV for better handling.

import pandas as pd

p1 = []
pro_des = []
p_l = []
p_c = []
p_d = []
p_url = []
p_url_ = []
final_df = pd.DataFrame()
# list_df = pd.read_csv("../MasterData/master_data.csv")

def sort_compare(list_df):
    for row in list_df.itertuples(index=True, name='Pandas'):
        # p = row.Product
        p=row.Product_Name
        pro_des.append(row.Product_Description)
        p1.append(p)
        p_url.append(row.Product_URL)

    file_df = pd.read_csv("comparision_data.csv").dropna()
    df1 = file_df.sort_values('Product_Line').drop_duplicates('Product_Line' , keep = 'last')
    # print(df1['Product_Line'])

    for row in df1.itertuples(index=True, name='Pandas'):
        pl = row.Product_Line
        pc = row.Product_Category
        for elem in p1:
            if elem == pl:
                pos = p1.index(elem)
                p_d.append(pro_des[pos])
                p_l.append(elem)
                p_c.append(pc)
                p_url_.append(p_url[pos])
            else:
                continue

    final_df['Product_Name'] = p_l
    final_df['Product_Category'] = p_c
    final_df['Product_Description'] = p_d
    final_df['Product_URL'] = p_url_
    # final_df.to_csv(r'compared_list_final.csv', sep=',', mode='a')
    return final_df