from bs4 import BeautifulSoup
from bs4.element import Comment
import urllib.request
import requests
import csv
import os
from tika import parser
import re
import pathlib
import unidecode
import pandas as pd
from Compare_Products.compare import sort_compare
from db import sqlEngine
from sqlalchemy.dialects.mysql import LONGTEXT
import time

start_time = time.time()

dtype = {
    "Page_Text": LONGTEXT,
    "File_Text": LONGTEXT
}

master_df = pd.DataFrame()
common_text = "  Javascript must be enabled for the correct page display   Skip to Content     Products  Industries  Services and Support  Training  Community  Partner  About   Products  Overview  ERP and Finance  ERP and Finance  Overview SAP S/4HANA ERP for Small and Midsize Enterprises Financial Planning and Analysis Accounting and Financial Close Treasury Management Accounts Receivable, Billing and Revenue Management Cybersecurity, Governance, Risk and Compliance   CRM and Customer Experience  CRM and Customer Experience  Overview SAP C/4HANA Customer Data Marketing Commerce Sales Service   Network and Spend Management  Network and Spend Management  Overview Supplier Management Strategic Sourcing Procurement Services Procurement and Contingent Workforce Selling and Fulfillment Travel and Expense   Digital Supply Chain  Digital Supply Chain  Overview Supply Chain Planning Supply Chain Logistics Manufacturing R&D / Engineering Asset Management   HR and People Engagement  HR and People Engagement  Overview Employee Experience Management Core HR and Payroll Talent Management HR Analytics and Workforce Planning   Business Technology Platform  Business Technology Platform  Database and Data Management Application Development and Integration Analytics Intelligent Technologies  Spotlights   Spotlights   Intelligent Enterprise   Experience Management   Digital Transformation   Small and Midsize Enterprises   Technology Trends  More...   Industries  Overview  Energy and Natural Resources  Energy and Natural Resources  Building Products Chemicals Mill Products Mining Oil and Gas Utilities   Financial Services  Financial Services  Banking Insurance   Consumer Industries  Consumer Industries  Agribusiness Consumer Products Fashion Life Sciences Retail Wholesale Distribution   Discrete Industries  Discrete Industries  Aerospace and Defense Automotive High Tech Industrial Machinery and Components   Service Industries  Service Industries  Airlines Engineering, Construction, and Operations Media Professional Services Railways Sports & Entertainment Telecommunications Travel and Transportation   Public Services  Public Services  Defense and Security Future Cities Healthcare Higher Education and Research Public Sector  Spotlights   Spotlights   Intelligent Enterprise   Experience Management   Digital Transformation   Small and Midsize Enterprises   Technology Trends  More...   Services and Support  Overview  Services and Support Offerings  Services and Support Offerings  Overview Innovation Services and Solutions Advisory Services Implementation Services Cloud Services Support Services Premium Success Engagements   SAP Support Portal  SAP Support Portal  Overview My Support Overview Knowledge Base Product Support Software Downloads SAP Support Strategy Application Lifecycle Management SAP ONE Support Launchpad Maintenance 2040   SAP Help Portal  SAP Help Portal  Overview Product Documentation Finder SAP Road Maps Best Practices   SAP for Me Customer Portal  SAP for Me Customer Portal  Login to SAP for Me  Spotlights   Spotlights   SAP Cloud ALM   SAP Solution Manager   SAP Focused Run   Training  Overview  Find Training  Find Training  Learning Journeys Training Courses SAP Global Certification SAP Learning Hub SAP Free Training with openSAP   Deploy, Adopt, and Maintain SAP Solutions  Deploy, Adopt, and Maintain SAP Solutions  Continuous Learning Framework from SAP Education Consulting Services SAP Education Partners  Spotlights   Spotlights   Register for a Training Course   Browse openSAP courses   Community  Overview  Community Overview  Community Overview  Overview Questions and Answers Browse Topics Blogs Community Events Community Programs Community Resources  Spotlights   Spotlights   What's New in the SAP Community   SAP Developer Center   Partner  Overview  Find a Partner  Find a Partner  Overview Award-Winning Partners Strategic Partners Extend Your Solution Certified Solutions and Hardware Outsourcing Partners SAP Crystal Solutions Resellers SAP Partner Finder   Become a Partner  Become a Partner  Overview Build Solutions Sell Solutions Service Solutions Run Solutions Apply Now   Certify My Solution  Certify My Solution  Overview Co-Innovated with SAP Software Certification Hardware Certification Outsourcing Partner Certification Technical Services   Already a Partner  Already a Partner  Overview Log In to SAPPartnerEdge.com  Spotlights   Spotlights   Find an SAP Partner   Apps from SAP Partners   SAP-Qualified Partner-Package Solutions   About  Overview  Global Company Information  Global Company Information  Overview Leadership History Purpose and Promise Diversity and Inclusion Sustainability and CSR Innovation Quality Global Sponsorships Worldwide Directory   Investor Relations  Investor Relations  Overview Capital Market Story Publications Calendar Stock News Governance Fixed Income   SAP Trust Center  SAP Trust Center  Overview Cloud Service Status Security Data Protection and Privacy Compliance Cloud Operations Data Center SAP Agreements   News and Press  News and Press  Overview Top Stories Press Room Press Contacts Media Coverage SAP-TV   Careers  Careers  Overview Who We Are University Your Career Joining SAP Job Search   Customer Engagement  Customer Engagement  Overview Customer Stories Customer First Customer Influence and Adoption SAP User Groups SAP Executive Briefing Centers   Events  Events  Overview SAPPHIRE NOW SAP TechEd Find an Event   About SAP North America  About SAP North America  Overview Management Team Corporate Social Responsibility Local Offices  Spotlights   Spotlights   Why SAP   Find an Event   Integrated Report   Job Search   Knowledge for SAP User Groups   Search  Clear  Search   Menu  Try & Buy  Search  Search  Clear  Search   Close    United States     Contact US     Contact Us  Call us at United States +1-800-872-1727  Or see our complete list of local country numbers     Call Me Now Call Offline SAP can call you to discuss any questions you have.     Chat Now Chat Offline Get live help and chat with an SAP representative.     Contact Us E-mail us with comments, questions or feedback.        "


def visible_tag(element):
    if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]', 'footer']:
        return False
    if isinstance(element, Comment):
        return False
    return True


def text_from_html(body):
    soup = BeautifulSoup(body, 'html.parser')
    texts = soup.findAll(text=True)
    visible_texts = filter(visible_tag, texts)  
    return u" ".join(t.strip() for t in visible_texts)


# Function to replace unicodes
def ReplaceUnicode(text):
    clean = unidecode.unidecode(text) # Replacing other unicodes
    clean = clean.replace("\u2009", " ")
    clean = clean.replace("\u00ae", " ") # registered
    clean = clean.replace("\u2022", " ")
    clean = clean.replace("\u00a9", " ") # copyright
    clean = clean.replace("\u201c", " ")
    clean = clean.replace("\u201d", " ")
    clean = clean.replace("\u2019", " ")
    clean = clean.replace("\u2013", " ")
    clean = clean.replace("\u200a", " ")
    clean = clean.replace("\u2014", " ")
    clean = clean.replace("\ufffd", " ")
    clean = clean.replace("\u2018", " ")
    clean = clean.replace("\u2003", " ")
    clean = clean.replace("\u2026", " ")
    clean = clean.replace("\u20ac", " ")
    clean = clean.replace("\u2122", " ")
    clean = clean.replace("\u2713", " ")
    clean = clean.replace("\u01c0", " ")
    clean = clean.replace("- ", "")
    clean = clean.replace("  ", " ")
    clean = clean.replace("  ", " ")
    
    clean = re.sub(r'\w+:\/{2}[\d\w-]+(\.[\d\w-]+)*(?:(?:\/[^\s/]*))*', '', clean) # Remove http urls
    return clean


# Write MasterData to CSV
def writeMasterData(name,text):
    filteredList = []
    for clean in text:
        clean_ = clean.replace(common_text, "")
        filteredList.append(clean_)
    path = './MasterData/'
    # Check if path exists. If not exists create new
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)
    with open(path + 'master_data.csv', 'w', encoding='utf-8-sig', newline='') as myfile:
        wr = csv.writer(myfile)
        wr.writerow(("Product_Name", "Product_Description"))
        wr.writerows(zip(name,filteredList))
    print("Task completed")


def write_to_db(data_frame, tableName):
    dbConnection = sqlEngine()
    print('Write to DB started: %s' % tableName)
    sql_query = 'ALTER TABLE ' + tableName + ' ADD PRIMARY KEY (id);'
    try:
        frame = data_frame.to_sql(tableName, dbConnection, if_exists='replace', dtype=dtype, index = True, index_label='id')
        dbConnection.execute(sql_query)
    except ValueError as vx:
        print(vx)
    except Exception as ex:
        print(ex)
    else:
        print("Table %s created successfully." % tableName)
    finally:
        dbConnection.close()


def main_func():
    page_text = []
    file_text = []
    master_text = []
    master_name = []
    product_url = []
    file_url = []
    product_category = []
    with open('product_list.csv', 'r') as file:
        reader = csv.reader(file)
        next(reader, None)
        for row in reader:
            name = row[0] # Assigning product name variable
            category = row[1] # Assigning product category
            url = row[2]  # Assigning product url to variable
            doc_link = row[3]

            html = urllib.request.urlopen(url).read()
            data = text_from_html(html) # Extracting relevant text from webpage
            data_ = ReplaceUnicode(data)
            # name_ = name.replace(" ","_") # Replacing whitespace in name string with '_' for easy name representation on dist
            # nameRep = name_.replace("/", "_")
            page_text.append('"' + data_ + '"')
            master_name.append(name)
            product_url.append(url)
            product_category.append(category)
            file_url.append(doc_link)
            if doc_link == 'empty':
                file_text.append('Not Available')
                continue
                
            else:
                response = requests.get(doc_link)
                name_ = name.replace(" ","_") #Replacing whitespace in name string with '_' for easy name representation on dist
                nameRep = name_.replace("/", "_")
                path = './PDFs/'
                #Check if path exists. If not exists create new
                pathlib.Path(path).mkdir(parents=True, exist_ok=True)
                doc_path = path + nameRep + '.pdf'
                with open(doc_path, 'wb') as f:
                    f.write(response.content)
                readFromdoc = parser.from_file(doc_path)
                data = readFromdoc['content']
                data_ = ReplaceUnicode(data)
                file_text.append('"' + data_ + '"')
                # master_name.append(name)
                # product_url.append(url)

    master_df['Product_Name'] = master_name
    master_df['Product_Category'] = product_category
    master_df['Page_Text'] = page_text
    master_df['File_Text'] = file_text
    master_df['Product_URL'] = product_url
    master_df['File_URL'] = file_url

    # writeMasterData(master_name,master_text)  
    # final_df = sort_compare(master_df)

    # writer = pd.ExcelWriter('example.xlsx', engine='xlsxwriter')
    # final_df.to_excel(writer, sheet_name='Sheet1')
    # writer.save()

    write_to_db(master_df, 'sap_data')
    print("Task completed in %s minutes " % ((time.time() - start_time) / 60))

# Main func
if __name__ == '__main__':
    main_func()
