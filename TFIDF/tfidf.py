import math
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from numpy import zeros

# master_df = pd.read_csv(".././product_list_pd.csv").dropna()
# # master_df = pd.read_csv("data.csv").dropna()
# input_df = pd.read_csv("input_string.csv").dropna()

def cosine_similarity(vector1, vector2):
    dot_product = sum(p * q for p, q in zip(vector1, vector2))
    magnitude = math.sqrt(sum([val ** 2 for val in vector1])) * math.sqrt(sum([val ** 2 for val in vector2]))
    if not magnitude:
        return 0
    return dot_product / magnitude


def tfidf_compute(master_df, input_df, master_df_col):

    #supplied string
    def doc_vec(doc):
        v = zeros(vector_length)
        for word in feature_names_query:
            if word in feature_names_master:
                index = feature_names_master.index(word)
                v[index] = 1
        return v

    vectorizer = TfidfVectorizer()
    vectors = vectorizer.fit_transform(master_df[master_df.columns[master_df_col]])
    feature_names_master = vectorizer.get_feature_names()
    dense = vectors.todense()
    denselist = dense.tolist()
    vector_length = len(feature_names_master)

    for j in range(1):
        query = input_df[input_df.columns[1]]
        vectorizer = TfidfVectorizer()
        vectors = vectorizer.fit_transform(query)
        feature_names_query = vectorizer.get_feature_names()
        query_vector = doc_vec(query)

        cosine_similarity_list = []
        for i in range(len(master_df)):
            similarity = cosine_similarity(query_vector, denselist[i])
            # print(similarity)
            cosine_similarity_list.append(similarity)

    master_df_new = master_df
    # master_df_new= master_df_new.drop(master_df_new.columns[0], axis=1) #Drop Unnamed: 0 column from df
    # del master_df_new['Product_Description']
    master_df_new['Cosine_Similarity'] = cosine_similarity_list
    master_df_new = master_df_new.sort_values(by='Cosine_Similarity',ascending=False)
    # master_df_new.to_csv(r'result1.csv', sep=',', mode='a')

    return master_df_new