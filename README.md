# Master's Thesis
> A Master Thesis topic to find suitable open-sourced alternatives to SAP products. 

## Requirements
    Python3
    pip
    MySQL

## Installation
Install the given dependencies with pip.
```
pip install beautifulsoup4 tika pandas scikit-learn sqlalchemy pymysql
```
Database configuration is available in `db.py`
```
sqlEngine = create_engine('mysql+pymysql://username:password@127.0.0.1/database_name', pool_recycle=3600)
```
To start data scraping, execute below command within the project directory.
```
$ python alternative_master_scrape.py
```

## API
Project has an inbuilt api (post method) to send result data with requested search phrase.

To start the server, execute command:
```
python api.py
```

URL: http://localhost:8080/search_result

Method: POST \
Request Sample: {
	"search_phrase" : "data management"
}



<!-- To run the project, execute below commands from the project directory. 
```
$ python data_extraction.py
```
This extracts data from the URLs present in `product_list.csv`. The data is extracted using Beautifulsoup from the webpages and using Tika parser from the PDFs. Downloaded PDFs are stored in directory `PDFs` and a csv file with collected data is stored in directory `MasterData`.  -->

<!-- ```
$ cd ./TFIDF
$ python tfidf.py
```
On the collected data we are performing "Term frequency and inverted documented frequency" to find the relativeness of given input string.

Corresponding results are stored in `./TFIDF/results.csv`

Feed the input string to corresponding column in `input_string.csv` before executing `tfidf.py`

`source_product_url.py` as of now does not include logic to extract URLs of PDF marketing document available for a given SAP product. The actual code doesn't complie complete list of pdf urls hence has been excluded from the code. A fix is underway.  -->