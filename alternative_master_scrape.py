from bs4 import BeautifulSoup
import csv
import requests
from datetime import date, timedelta
import pandas as pd
import time
import urllib.request
import re
from db import sqlEngine
from data_extraction import text_from_html
from sqlalchemy.dialects.mysql import LONGTEXT
from fake_useragent import UserAgent
ua = UserAgent()

dtype = {
    "Project_Description": LONGTEXT,
    "Page_Text": LONGTEXT,
}

# Initializing dataframe
master_alternative_df = pd.DataFrame()
table1_df = pd.DataFrame()
table2_df = pd.DataFrame()
# Time to calculate task process time
start_time = time.time()
# Initialize domain
domain = 'https://sourceforge.net'
# Initialize lists
project_name = []
project_page = []
project_last_update = []
project_category = []
project_description = []
project_features = []
project_url = []
page_text = []
pricing_info = []

# Regex to validate URLs
regex = re.compile(
    r'^(?:http|ftp)s?://'  # http:// or https://
    r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
    r'localhost|'  # localhost...
    r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
    r'(?::\d+)?'  # optional port
    r'(?:/?|[/?]\S+)$', re.IGNORECASE)


# Function to decode time from text to date
def getText(text_string):
    texts = ''.join([i.text for i in text_string if i])
    if 'days ago' in texts:
        d_days = texts.replace(" days ago", "")
        newDate = date.today() - timedelta(days=int(d_days))
        return str(newDate)
    elif 'day ago' in texts:
        d_days = texts.replace(" day ago", "")
        newDate = date.today() - timedelta(days=int(d_days))
        return str(newDate)
    elif 'minutes ago' in texts:
        d_days = texts.replace(" minutes ago", "")
        newDate = date.today() - timedelta(minutes=int(d_days))
        return str(newDate)
    elif 'minute ago' in texts:
        d_days = texts.replace(" minute ago", "")
        newDate = date.today() - timedelta(minutes=int(d_days))
        return str(newDate)
    elif 'hours ago' in texts:
        d_hours = texts.replace(" hours ago", "")
        newDate = date.today() - timedelta(hours=int(d_hours))
        return str(newDate)
    else:
        return texts


# Fetch required data from URLs
def fetch_data(url):
    # defining headers for url request
    header = {'User-Agent':str(ua.random)}
    print('Project URL: ' + url)
    page = requests.get(domain + url)  # open URl
    soup = BeautifulSoup(page.text, 'html.parser')  # Parse data using beautifulsoup
    description = getText(soup.select('p.description'))  # extract description
    features = soup.select('.has-feature')  # extract features
    pricing = getText(soup.select('.m-subgroup-1 .m-sub-section:nth-child(1)')) # Pricing information

    if pricing:
        pricing_info.append(pricing)
        # print(pricing)
    else:
        pricing_info.append('Not available')
        # print('Not Available')

    if features:
        features_ = " ".join('|'.join([i.text for i in features if i]).split())# Separating features by '|' and remove unnecessary white spaces.

    elif not features:
        features = soup.select('.feature')
        features_ = '|'.join([i.text for i in features if i])    # Separating features by '|' 
    else:
        features_ = "No features found"
    # href_link_commercial = getText(soup.select('.field-row:nth-child(5)'))
    link_text = getText(soup.select('.m-subgroup-1'))

    regex_url = re.compile(r'\b((?:https?://)?(?:(?:www\.)?(?:[\da-z\.-]+)\.(?:[a-z]{2,6})|(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)|(?:(?:[0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,7}:|(?:[0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,5}(?::[0-9a-fA-F]{1,4}){1,2}|(?:[0-9a-fA-F]{1,4}:){1,4}(?::[0-9a-fA-F]{1,4}){1,3}|(?:[0-9a-fA-F]{1,4}:){1,3}(?::[0-9a-fA-F]{1,4}){1,4}|(?:[0-9a-fA-F]{1,4}:){1,2}(?::[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:(?:(?::[0-9a-fA-F]{1,4}){1,6})|:(?:(?::[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(?::[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(?:ffff(?::0{1,4}){0,1}:){0,1}(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])|(?:[0-9a-fA-F]{1,4}:){1,4}:(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])))(?::[0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])?(?:/[\w\.-]*)*/?)\b')
    url_link_from_text = re.findall(regex_url, link_text)

    href_link = soup.select('#homepage')
    # Check if href_link tag exists in page
    if href_link:
        # Checking in url contains http:// or https://
        if 'http://' in href_link[0]['href']:
            # link = href_link[0]['href'].replace('http://', 'https://')
            project_url.append(href_link[0]['href'])  # Append url to list
            project_url_scrape(href_link[0]['href'])  # Passing URL to project_url_scrape to get text from landing page

        elif 'https://' in href_link[0]['href']:
            project_url.append(href_link[0]['href'])  # Append url to list
            project_url_scrape(href_link[0]['href'])  # Passing URL to project_url_scrape to get text from landing page

        else:
            link = 'https://' + href_link[0]['href']
            project_url.append(link)  # Append url to list
            project_url_scrape(link)  # Passing URL to project_url_scrape to get text from landing page
        # project_url.append(href_link[0]['href'])
    # elif href_link_commercial:
    #     print(href_link_commercial)
    #     if 'http://' in href_link_commercial:
    #         link = href_link_commercial.replace('http://', 'https://')
    #         project_url.append(link)  # Append url to list
    #         project_url_scrape(link)  # Passing URL to project_url_scrape to get text from landing page

    #     elif 'https://' in href_link_commercial:
    #         project_url.append(href_link_commercial)  # Append url to list
    #         project_url_scrape(href_link_commercial)  # Passing URL to project_url_scrape to get text from landing page

    #     else:
    #         link = 'https://' + href_link_commercial
    #         print(link)
    #         project_url.append(link)  # Append url to list
    #         project_url_scrape(link)  # Passing URL to project_url_scrape to get text from landing page

    # Check for null list
    elif not url_link_from_text:
            project_url.append('NA')
            project_url_scrape('NA')

    elif url_link_from_text[-1]:
        # print(url_link_from_text[-1])

        if 'http://' in url_link_from_text[-1]:
            link = url_link_from_text[-1].replace('http://', 'https://')
            project_url.append(link)  # Append url to list
            project_url_scrape(link)  # Passing URL to project_url_scrape to get text from landing page

        elif 'https://' in url_link_from_text[-1]:
            project_url.append(url_link_from_text[-1])  # Append url to list
            project_url_scrape(url_link_from_text[-1])  # Passing URL to project_url_scrape to get text from landing page

        else:
            link = 'https://' + url_link_from_text[-1]
            # print(link)
            project_url.append(link)  # Append url to list
            project_url_scrape(link)  # Passing URL to project_url_scrape to get text from landing page
    else:
        project_url.append('NA')
        project_url_scrape('NA')
    project_description.append(description)  # Append desctiption to list
    project_features.append(features_ )  # Append features to list


# Function to get text from product URLs
def project_url_scrape(url):
    # defining headers for url request
    header = {'User-Agent':str(ua.chrome)}
    # Checking if url is in valid format    
    url_check = re.match(regex, url) is not None
    if url_check == True:
        try:
            response = requests.get(url, headers=header, timeout=5)
            data = text_from_html(response.text)
            page_text.append(data)
        except:
            print('Exception Occured')
            page_text.append("Page not reachable")

        # data = text_from_html(response.text)
        # page_text.append('"' + data + '"')
    else:
        page_text.append('Incorrect website')


# Function to get basic page data
def scrape_func(url, category):
    # defining headers for url request
    header = {'User-Agent':str(ua.random)}
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    project_list = soup.select('.projects li')

    for elem in project_list:
        tag = getText(elem.select('h3'))
        if tag:
            project_name.append(tag)
            see_project = elem.select('a.see-project')[0]['href']
            project_page.append(domain + see_project)
            date_time = getText(elem.select('.dateUpdated'))
            if date_time:
                project_last_update.append(date_time)
                # print(date_time)
            else:
                project_last_update.append('Commercial')
                # print('Commercial')
            project_category.append(category)

            fetch_data(see_project)

        else:
            continue


# Function to write data from dataframe to DB
def write_to_db(data_frame, tableName):
    dbConnection = sqlEngine()
    print('Write to DB started: %s' % tableName)
    # sql_query = 'ALTER TABLE ' + tableName + ' ADD PRIMARY KEY (id);'
    try:
        # frame = data_frame.to_sql(tableName, dbConnection, if_exists='append', dtype=dtype, index = True, index_label='id')
        frame = data_frame.to_sql(tableName, dbConnection, if_exists='append', index = False)

        # dbConnection.execute(sql_query)
    except ValueError as vx:
        print(vx)
    except Exception as ex:
        print(ex)
    else:
        print("Table %s created successfully." % tableName)
    finally:
        dbConnection.close()


# Main function
def main_func():
    # Calling scrape_func in loops to fetch data
    for x in range(1,36):
        url = 'https://sourceforge.net/directory/business-enterprise/enterprise/crm/?page=' + str(x)
        print("CRM :", x)
        scrape_func(url, 'CRM')
        # time.sleep(1)

    for x in range(1,38):
        url = 'https://sourceforge.net/directory/business-enterprise/enterprise/erp/?page=' + str(x)
        print("ERP :", x)
        scrape_func(url, 'ERP')
        # time.sleep(1)

    for x in range(1,17):
        url = 'https://sourceforge.net/directory/business-enterprise/enterprise/data-warehousing/?page=' + str(x)
        print("Data Management :", x)
        scrape_func(url, 'Data Management')
        # time.sleep(1)

    for x in range(1,18):
        url = 'https://sourceforge.net/directory/business-enterprise/enterprise/enterprisebi/?page=' + str(x)
        print("Analytics :", x)
        scrape_func(url, 'Analytics') 
        # time.sleep(1)

    for x in range(1,164):
        url = 'https://sourceforge.net/directory/business-enterprise/financial/?page=' + str(x)
        print("Financial Management :", x + 1)
        scrape_func(url, 'Financial Management')
        # time.sleep(1)

    for x in range(1,8):
        url = 'https://sourceforge.net/directory/business-enterprise/enterprise/humanresources/?page=' + str(x)
        print("HCM :", x)
        scrape_func(url, 'Human Capital Management')
        # time.sleep(1)

    for x in range(1,3):
        url = 'https://sourceforge.net/directory/business-enterprise/enterprise/plm/?page=' + str(x)
        print("PLM :", x)
        scrape_func(url, 'Product Lifecycle Management')
        # time.sleep(1)

# Commercial Software
    for x in range(1,155):
        url = ' https://sourceforge.net/software/crm/?page=' + str(x)
        print("CRM :", x)
        scrape_func(url, 'CRM')
        # time.sleep(1)

    for x in range(1,157):
        url = 'https://sourceforge.net/software/finance/?page=' + str(x)
        print("Financial Management :", x)
        scrape_func(url, 'Financial Management')
        # time.sleep(1)

    for x in range(1,98):
        url = 'https://sourceforge.net/software/business-intelligence/?page=' + str(x)
        print("Analytics :", x)
        scrape_func(url, 'Analytics')
        # time.sleep(1)

    for x in range(1,100):
        url = 'https://sourceforge.net/software/collaboration/?page=' + str(x)
        print("Content and Collaboration :", x)
        scrape_func(url, 'Content and Collaboration')
        # time.sleep(1)

    for x in range(1,100):
        url = 'https://sourceforge.net/software/supply-chain-management/?page=' + str(x)
        print("Supply Chain Management :", x)
        scrape_func(url, 'Supply Chain Management')
        # time.sleep(1)

    for x in range(1,7):
        url = 'https://sourceforge.net/software/manufacturing/?page=' + str(x)
        print("Digital Manufacturing :", x)
        scrape_func(url, 'Digital Manufacturing')
        # time.sleep(1)

    for x in range(1,187):
        url = 'https://sourceforge.net/software/human-resources/?page=' + str(x)
        print("Human Capital Management :", x)
        scrape_func(url, 'Human Capital Management')
        # time.sleep(1)

    for x in range(1,9):
        url = 'https://sourceforge.net/software/database-management/?page=' + str(x)
        print("Human Capital Management :", x)
        scrape_func(url, 'Data Management')
        # time.sleep(1)

    for x in range(1,6):
        url = 'https://sourceforge.net/software/product-lifecycle-management/?page=' + str(x)
        print("PLM :", x)
        scrape_func(url, 'Product Lifecycle Management')
        # time.sleep(1)

    for x in range(1,25):
        url = 'https://sourceforge.net/software/erp/?page=' + str(x)
        print("ERP :", x)
        scrape_func(url, 'ERP')
        # time.sleep(1)
    
    for x in range(1,33):
        url = 'https://sourceforge.net/software/ecommerce/?page=' + str(x)
        print("Supplier Relationship Management :", x)
        scrape_func(url, 'Supplier Relationship Management')
        # time.sleep(1)

    for x in range(1,10):
        url = 'https://sourceforge.net/software/database-management/?page=' + str(x)
        print("Technology Platform :", x)
        scrape_func(url, 'Technology Platform')
        # time.sleep(1)

    for x in range(1,41):
        url = 'https://sourceforge.net/software/application-development/?page=' + str(x)
        print("Technology Platform :", x)
        scrape_func(url, 'Technology Platform')
        # time.sleep(1)
    

    # scrape_func('https://sourceforge.net/software/product-lifecycle-management/?page=2', 'Try')
    # scrape_func('https://sourceforge.net/directory/business-enterprise/enterprise/plm/?page=2', 'Try')

    table1_df['Product_Name'] = project_name
    table1_df['Project_Category'] = project_category

    # Removing duplicate values from table1
    table1__df = table1_df.drop_duplicates(subset=['Product_Name', 'Project_Category'], keep='first')
    # Reset index DataFrame
    table1__df = table1__df.reset_index(drop=True)

    table2_df['Product_Name'] = project_name
    table2_df['Project_Page'] = project_page
    table2_df['Project_Description'] = project_description
    table2_df['Project_Features'] = project_features
    table2_df['Project_URL'] = project_url
    table2_df['Page_Text'] = page_text
    table2_df['Project_Last_Update'] = project_last_update
    table2_df['Pricing_Information'] = pricing_info

    # Remove duplicate entries from DataFrame
    table2__df = table2_df.drop_duplicates(subset='Product_Name', keep='first')
    # Reset index DataFrame
    table2__df = table2__df.reset_index(drop=True)


    # write dataframe to database.
    write_to_db(table1__df, 'alternate_category')
    write_to_db(table2__df, 'alternative_products')
    print("Task completed in %s minutes " % ((time.time() - start_time) / 60))


if __name__ == '__main__':
    main_func()