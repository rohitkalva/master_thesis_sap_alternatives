import pandas as pd
from TFIDF.tfidf import tfidf_compute
from db import sqlEngine
import time
from nltk.corpus import stopwords 
from nltk.stem import PorterStemmer 
from nltk.stem import WordNetLemmatizer 
from nltk.tokenize import word_tokenize 
import json

start_time = time.time()
# main_df = pd.DataFrame()


def fetch_data_df(sql_query):
    dbConnection = sqlEngine()
    try:
        print("Start reading table")
        frame = pd.read_sql(sql_query, dbConnection, )
        # dbConnection.execute(sql_query)
    except ValueError as vx:
        print(vx)
    except Exception as ex:
        print(ex)
    else:
        print("Table read successfully.")
    finally:
        dbConnection.close()
    
    return frame


def search_input_df(search_string):
    input_string = {'Name': ['String'], 'Phrase': [search_string]}
    input_df = pd.DataFrame(input_string)

    return input_df


# Function to remove stop words from given text. Based on NLTK library
def remove_stop_words(text):
    stop_words = set(stopwords.words('english')) 
    ps = PorterStemmer() 
    lemmatizer = WordNetLemmatizer() 
    word_tokens = word_tokenize(text) 
    
    filtered_sentence = [w for w in word_tokens if not w in stop_words] 
    
    filtered_sentence = [] 
    
    for w in word_tokens: 
        if w not in stop_words: 
            filtered_sentence.append(w) 

    filtered_text = []
    for w in filtered_sentence: 
        filtered_text.append(lemmatizer.lemmatize(w))
    
    filtered_text = ' '.join(str(e) for e in filtered_text)

    return filtered_text


# Filters the main dataframe and converts it into json 
def json_formatter(df):
     # Sorting cosine values in descending order
    main_df = df.sort_values(by='Cosine_Similarity', ascending=False)

    # Removing duplicate product names and its values keeping the first occurence. Considers the product with higher cosine value
    main_df = main_df.drop_duplicates(subset='product_name', keep='first')
    # main_df.to_csv(r'compared.csv', sep=',', mode='a')

    # Resetting index values after sort and duplicate removal operation
    main_df = main_df.reset_index(drop=True)
    # main_df = main_df.drop(['project_description'],['project_features'],['page_text'])
    # del main_df['project_description']  # deleting project_description column for better presentation
    # del main_df['project_features']  # deleting project_features column for better presentation
    del main_df['page_text']  # deleting page_textcolumn for better presentation
    # main_df.to_csv(r'resulsassat.csv', sep=',', mode='a')

    # formatting resultant df to json string. head limit for number of results
    json_df = main_df.head(10).to_json(orient='records')

    print("Task completed in %s minutes " % ((time.time() - start_time) / 60))
    return json_df


def compare(search_phrase, product_features):

    # Initialize variable
    temp_str = "SAP"
    # part of sql query
    add_string = "AND a.product_name not like '%%%%%s%%%%' AND (" % (temp_str)
    # Sql query part to append project_features in search for the count of product features entered with initial phrase.
    add_project_features = "a.project_features like '%%%%%s%%%%' " 
    query_ = ''
    # appending the string with search for project_features
    for i in range(len(product_features)):
        if i == 0:
            query_ = add_project_features % product_features[i].lstrip(' ')
        else:
            query_ = query_ + 'OR ' + add_project_features % product_features[i].lstrip(' ')


    # search_phrase = 'SAP Audit Management'
    query = "select * from sap_data"
    df = fetch_data_df(query)
    search_phrase_1 = search_input_df(search_phrase)
    new_pd = tfidf_compute(df, search_phrase_1, 1)  # for sap products
    # new_pd.to_csv(r'result1.csv', sep=',', mode='a')
    print(new_pd.iloc[0][1])
    print(new_pd.iloc[0][2])

    # Considering file text when available and removing stop words
    if new_pd.iloc[0][4] != 'Not Available':
        sap_desc = remove_stop_words(new_pd.iloc[0][4])
        # sap_desc = new_pd.iloc[0][4]
    else:
        sap_desc = remove_stop_words(new_pd.iloc[0][3])
        # sap_desc = new_pd.iloc[0][3]

    # print(new_pd.iloc[0][3])
    # time.sleep(60)
    mylist = new_pd.iloc[0][2]
    main_df = pd.DataFrame([])
    unavailable_list = ['NA']
 
    # Conditional statemnet to fetch data corresponding to retreived category
    if mylist == 'Enterprise Management':
        # query_2 = "select a.product_name, a.project_description, a.project_features, a.page_text, a.project_last_update, b.project_category from alternative_products a JOIN alternate_category b where a.product_name = b.product_name AND b.project_category = 'ERP' AND a.project_last_update > '2017-01-01'  AND a.project_last_update != 'commercial';"
        query_2 = "select a.product_name, a.project_description, a.project_features, a.page_text, a.project_last_update, b.project_category, a.Pricing_Information \
                    from alternative_products a JOIN alternate_category b \
                    WHERE b.project_category = 'ERP' AND a.product_name = b.product_name AND a.project_last_update > '2017-01-01' " + add_string + query_  + ');'
        alternative_df = fetch_data_df(query_2)
        # TFIDF output df after comparing sap product description with alternative product description
        alt_description_df = tfidf_compute(alternative_df, search_input_df(sap_desc), 1)
        # TFIDF output df after comparing sap product description with alternative product features 
        alt_feature_df = tfidf_compute(alternative_df, search_input_df(sap_desc), 2)
        # TFIDF output df after comparing sap product description with alternative product page text
        alt_pagetext_df = tfidf_compute(alternative_df, search_input_df(sap_desc), 3)

        # Appending TFIDF results to main dataframe
        main_df = main_df.append(alt_description_df, sort=False)
        main_df = main_df.append(alt_feature_df, sort=False)
        main_df = main_df.append(alt_pagetext_df, sort=False)

        return json_formatter(main_df)
    
    
    elif mylist == 'Customer Relationship Management':
        query_2 = "select a.product_name, a.project_description, a.project_features, a.page_text, a.project_last_update, b.project_category, a.Pricing_Information \
                    from alternative_products a JOIN alternate_category b \
                    WHERE b.project_category = 'CRM' AND a.product_name = b.product_name AND a.project_last_update > '2017-01-01' " + add_string + query_  + ');'
        alternative_df = fetch_data_df(query_2)
        # TFIDF output df after comparing sap product description with alternative product description
        alt_description_df = tfidf_compute(alternative_df, search_input_df(sap_desc), 1)
        # TFIDF output df after comparing sap product description with alternative product features
        alt_feature_df = tfidf_compute(alternative_df, search_input_df(sap_desc), 2)
        # TFIDF output df after comparing sap product description with alternative product page text
        alt_pagetext_df = tfidf_compute(alternative_df, search_input_df(sap_desc), 3)

        # Appending TFIDF results to main dataframe
        main_df = main_df.append(alt_description_df, sort=False)
        main_df = main_df.append(alt_feature_df, sort=False)
        main_df = main_df.append(alt_pagetext_df, sort=False)

        return json_formatter(main_df)
    
    elif mylist not in unavailable_list:
        query_2 = "select a.product_name, a.project_description, a.project_features, a.page_text, a.project_last_update, b.project_category, a.Pricing_Information \
                    from alternative_products a JOIN alternate_category b \
                    WHERE b.project_category ='" + mylist + "' AND a.product_name = b.product_name AND a.project_last_update > '2017-01-01' " + add_string + query_  + ');'
        alternative_df = fetch_data_df(query_2)
        # TFIDF output df after comparing sap product description with alternative product description
        alt_description_df = tfidf_compute(alternative_df, search_input_df(sap_desc), 1)
        # TFIDF output df after comparing sap product description with alternative product features
        alt_feature_df = tfidf_compute(alternative_df, search_input_df(sap_desc), 2)
        # TFIDF output df after comparing sap product description with alternative product page text
        alt_pagetext_df = tfidf_compute(alternative_df, search_input_df(sap_desc), 3)

        # Appending TFIDF results to main dataframe
        main_df = main_df.append(alt_description_df, sort=False)
        main_df = main_df.append(alt_feature_df, sort=False)
        main_df = main_df.append(alt_pagetext_df, sort=False)
        return json_formatter(main_df)

    else:
        json_resp = {
                    'message': 'Suitable alternative not found'
                    }
        print("Task completed in %s minutes " % ((time.time() - start_time) / 60))
        return json.dumps(json_resp)


if __name__ == '__main__':
    compare('phrase')
