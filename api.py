from flask import jsonify
from flask import flash, request, Flask
from flask_cors import CORS, cross_origin
from comparision import compare, fetch_data_df
from doc2vec import doc2vec

# Init app
app = Flask(__name__)
#Enable CORS
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

# Base route to check connectivity
@app.route('/ping', methods=['GET'])
@cross_origin()
def ping():
    message = {
        'message': 'Internet is connected'
    }
    resp = jsonify(message)
    resp.status_code = 200
    return resp


# Main route for search string and corresponding result
@app.route('/search_result_tfidf', methods=['POST'])
@cross_origin()
def search_result_tfidf():
    try:
        search_phrase = request.json['search_phrase']
        print(search_phrase)
        product_phrase = search_phrase.split("|")
        product_features = product_phrase[1].split(',')
        resp = compare(product_phrase[0].rstrip(), product_features)

        return resp
    except:
        message = {
            'message': 'Error occured'
        }

        resp = jsonify(message)
        resp.status_code = 200
        return resp

@app.route('/search_result_doc2vec', methods=['POST'])
@cross_origin()
def search_result_doc2vec():
    try:
        search_phrase = request.json['search_phrase']
        print(search_phrase)
        product_phrase = search_phrase.split("|")
        product_features = product_phrase[1].split(',')
        resp = doc2vec(product_phrase[0].rstrip(), product_features)

        return resp
    except:
        message = {
            'message': 'Error occured'
        }

        resp = jsonify(message)
        resp.status_code = 200
        return resp

#GET endpoint for sap products list
@app.route('/sap_products_list', methods=['GET'])
@cross_origin()
def sap_products_list():
    productList = []
    products_list = fetch_data_df("select product_name from sap_data")
    for elem in range(len(products_list)):
        productList.append(products_list.iloc[elem][0])
       
    message = {
        'sap_products_list': productList
    }
    resp = jsonify(message)
    resp.status_code = 200
    return resp

# Error Handler
@app.errorhandler(404)
@cross_origin()
def not_found(error=None):
    message = {
        "status": 404,
        "message": "Not Found: " + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)
