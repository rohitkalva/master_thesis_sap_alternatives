import requests
from bs4 import BeautifulSoup
import csv

def main_func():
    URL = 'https://www.sap.com/products-a-z.html'
    page = requests.get(URL)

    soup = BeautifulSoup(page.text, 'html.parser')

    # Pull text from all instances of <a> tag within BodyText div
    listing = soup.select(".he a")
    # print(listing)
    name = []
    link = []
    for elems in listing:
        arr = {
            "product": elems.get_text(),
            "url": elems['href']
        }
        name.append(elems.get_text())
        link.append('http:'+elems['href'])
    
    write_csv(name,link)

def write_csv(name,link):
    with open('product_list.csv', 'w', encoding="ISO-8859-1", newline='') as myfile:
        wr = csv.writer(myfile)
        wr.writerow(("Product", "URL"))
        wr.writerows(zip(name,link))

if __name__ == '__main__':
    main_func()